FROM eclipse-temurin:21.0.6_7-jdk-alpine AS temurin
FROM maven:3.9.9-eclipse-temurin-21-alpine AS maven

FROM docker:28.0.1-dind

# Copy from Java base
ENV JAVA_HOME /opt/java/openjdk
ENV PATH $JAVA_HOME/bin:$PATH
COPY --from=temurin $JAVA_HOME $JAVA_HOME

# useful stuff from temurin image
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

# fontconfig and ttf-dejavu added to support serverside image generation by Java programs
RUN apk add --no-cache fontconfig libretls musl-locales musl-locales-lang ttf-dejavu tzdata zlib \
    && rm -rf /var/cache/apk/*


# Copy from Maven base
ENV MAVEN_HOME /usr/share/maven
ENV PATH $MAVEN_HOME/bin:$PATH
COPY --from=maven $MAVEN_HOME $MAVEN_HOME

RUN echo Verifying install ... \
    && echo javac --version && javac --version \
    && echo java --version && java --version \
    && echo mvn --version && mvn --version \
    && echo Complete.


# CE specific:
RUN apk update && apk --no-cache upgrade && apk --no-cache add bash curl unzip \
     && rm -rf /var/cache/apk/* *.apk

